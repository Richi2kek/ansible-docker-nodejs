# TP DOCKER/ANSIBLE

Créer plusieur serveurs ssh et y deployer nginx nodejs et express de facon automatique avec ansible et docker.

## Créer les conteneurs docker:

cf instructions tp:
a) login au hub privé.
b) récupérer l'image du serveur OpenSSH
c) Creer 3 conteneurs docker ou plus:

```bash
$ sudo docker run -d --name="ssh-server-1" registry.mrzee.fr/ynov/ssh-server:0.1
$ sudo docker run -d --name="ssh-server-2" registry.mrzee.fr/ynov/ssh-server:0.1
$ sudo docker run -d --name="ssh-server-3" registry.mrzee.fr/ynov/ssh-server:0.1
```
d) Vous pouvez récupérer l'ip de vos conteneurs avec la commande:

```bash
$ sudo docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ssh-server-1
$ sudo docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ssh-server-2
$ sudo docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ssh-server-3
```

e) Ajouter la clé ssh a votre agent ssh:

```bash
$ ssh-add /path/to/shh_pub.key
```
f) Connectez vous une fois à chacun de vos serveurs pour accepter le fingerprint

```bash
$ ssh root@<Container_IP>
$ yes
```

i) Ajoutez les ips de vos conteneurs au fichier hosts d'ansible et creer un groupe avec:

```bash
$ sudo nano /etc/ansible/hosts

```
ici le groupe est targets.

j) testez d'abord si la connexion avec les serveur est bonne:

```bash
$ ansible all -m ping -vvv
```

k) Si tout ping correctement vous pouvez lancer le playbook pour automatiser l'installation de nginx nodejs et express:

```bash
$ ansible-playbook playbook.yml
```

l) Testez que tout est ok, allez sur votre navigateur préféré et rendez vous à l'adress de vos conteneur ave comme port le 80 et vous decvriez voir en gros :
"Express Welcome to express"

m) Pour supprimer  facilement vos conteneurs:

```bash
$ sudo docker stop $(sudo docker ps -aq)
```
puis 

```bash
$ sudo docker rm $(sudo docker ps -aq)
```
